import React from 'react';
import SearchBar from "react-native-dynamic-search-bar";

export default SearchBarView = () =>{

    const [searchQuery, setSearchQuery] = React.useState('');

    const onChangeSearch = query => setSearchQuery(query);

    return (
        <SearchBar
            placeholder="Search here"
            onChangeText={onChangeSearch}
            value={searchQuery}
        />
    )
};