import React from "react";

import {View, Animated, Dimensions, StyleSheet} from "react-native";

const {width} = Dimensions.get("screen");

export default Paginator = ({data , scrollX}) => {

    return (

        <View style = {styles.container}>
            {
                data.map((_,i) => {
                    
                    const inputRange = [(i - 1) * width, i * width, (i+1) * width];

                    const dotWidth = scrollX.interpolate({
                        inputRange, 
                        outputRange: [10,20,10],
                        extrapolate: 'clamp'
                    });

                    const opacity = scrollX.interpolate({
                        inputRange,
                        outputRange: [0.3,1,0.3],
                        extrapolate: 'clamp'
                    });
                    
                    return(
                        <Animated.View 
                            style ={[styles.dot,{width: dotWidth, opacity}]} 
                        >
                        </Animated.View>
                    )
                }

                ) 
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems:'center',
        flexDirection: 'row',
        bottom: 20
    },
    dot:{
        height:10,
        borderRadius:5,
        backgroundColor: '#284b63',
        marginHorizontal: 8,
        width:10,
    }
})