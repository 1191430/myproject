import React from 'react';

import {View, Text, StyleSheet, Dimensions} from 'react-native';

const width = Dimensions.get('window').width;

export default OnboardingItem = ({item}) =>{

    return(
        <View style={styles.container}>
            <View style = {styles.containerBigTitle}>
                <Text style={styles.bigTitle}>{item.bigTitle}</Text>
            </View>
            <View style = {styles.containerText}>
                <Text style = {styles.title} >{item.title}</Text>
                <Text style = {styles.text} >{item.description}</Text>
                <Text style = {styles.title} >{item.title2}</Text>
                <Text style = {styles.text} >{item.description2}</Text>
                <Text style = {styles.title} >{item.title3}</Text>
                <Text style = {styles.text} >{item.description3}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({

    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        width: width,
    },
    containerBigTitle:{
        flex: 0.07,
        textAlign: 'center',
    },
    containerText:{
        flex: 0.8,
        width: width-20,
    },
    bigTitle:{
        fontWeight: 'bold',
        fontSize: 22,
        textAlign: 'left',
        justifyContent: 'flex-start',
        color:'black',
        flexDirection:'row',
    },
    text:{
        color: 'grey',
        textAlign: 'justify',
        justifyContent: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 15,
        textAlign: 'left',
        marginBottom: 10,
        color: 'black',
        marginTop: 20
    }
})