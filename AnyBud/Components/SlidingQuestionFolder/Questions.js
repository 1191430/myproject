export default [
    {
        id: 1,
        bigTitle:'Informações Fornecidas Por Si',
        title:'Localização',
        description:'A Anybody recolherá a sua informação relativa à sua localização estritamente quando for necessário criar o evento. Assim como recolherá a localização dos utilizadores do grupo a quem quer enviar a notificação apenas para assegurar que estão numa distância acessível ao seu convite.',
        title2:'Lista de contactos',
        description2:'Em relação aos contactos, a Anybody apenas utilizará a sua lista de contactos com o intuito de uma importação de amigos direta, sem a necessidade de adiciona-los manualmente. Se algum dos contactos não estiver associado aos nossos serviços nenhuma informação sobre ele será mantida, pelo que será eliminada.',
        title3:'Dados pessoais',
        description3:'As informações que disponibiliza perante os seus dados serão mantidas em segurança, a Anybody utiliza encriptação nos seus serviços para assegurar que os seus dados estão seguros.'
    },
    {
        id: 2,
        bigTitle:'Segurança',
        title: 'Estou seguro a utilizar esta aplicação?',
        description: 'Sim, a Anybody não saberá onde é que os seus utilizadores estão, isto porque sempre que seja necessário recorrer à sua localização, esta estará encriptada, não sendo permitido visualizar.',
        title2: 'Todos os utilizadores poderão ver onde estou?',
        description2: 'Não, a Anybody assegura que nenhum utilizador saberá a localização de outro, neste caso, o utilizador só saberá que por volta da hora do evento os outros utilizadores estarão no local onde defeniu para se encontrarem. O mesmo se aplica para os outros utilizadores que receberem o convite, só saberão que o utilizador que organizou o evento estará naquele sítio à hora marcada.',
        title3: 'Tenha cuidado com a sua lista de contactos',
        description3:'A Anybody assegura a sua segurança em termos de localização, o mesmo não se específica para a sua rede de amigos. Ao marcar os seus eventos assegure-se que está a convidar as pessoas corretas e não adicione pessoas que não conhece pessoal, essas podem tomar vantagem da sua boa vontade.'
    },
    {
        id: 3,
        bigTitle:'Informações Recolhidas',
        title:'Informações Gerais de Localização',
        description:'Mesmo se optar por não utilizar as nossas funcionalidades relacionadas com a localização exata, utilizamos endereços IP e outras informações, como indicativos de número de telemóvel, para calcular a sua localização geral (por exemplo, país).',
        title2:'Escolhas do Utilizador',
        description2:'Recolhemos informações sobre as suas definições da aplicação, definições de privacidade e registos sobre quando aceitou os nossos Termos.',
        title3:'Informações de Registo e Resolução de Problemas',
        description3:'Recolhemos informações sobre o desempenho dos nossos Serviços durante a sua utilização dos mesmos, como informações de desempenho e diagnóstico relacionadas com o serviço. Estas informações incluem ficheiros de registo, registos de horas, dados de falhas ou de diagnóstico, registos de desempenho do site e relatórios ou mensagens de erros.'
    }
    
]