import React from 'react';

import HomeView from './HomeView';

export default HomeController = ({navigation}) => {

    // previne o retrocesso ao screen anterior
    React.useEffect(
        () => navigation.addListener('beforeRemove', (e) => {
            e.preventDefault();
        })
    )

    return(
        
        <HomeView navigation={navigation} />
        
    )
}