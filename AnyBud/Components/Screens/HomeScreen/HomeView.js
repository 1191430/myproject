import React from 'react';

import{
    View,
    Text,
    Button,
    StyleSheet,
    Dimensions,
    Image,
    StatusBar,
    TextInput,
    TouchableOpacity,
} from 'react-native';

import AntIcon from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import MaterialIcons from "react-native-vector-icons/MaterialIcons"

export default HomeView = ({navigation}) => {

    return(
        <View style={styles.container}>

            <StatusBar 
                backgroundColor = '#33b0b7' 
                barStyle='light-content'
            >
            </StatusBar>

            <View style={styles.header}>

                <View style={styles.container2}>

                    <Text style={styles.title}> Anybody </Text>

                    <TouchableOpacity>
                        <Feather style={{left: 3}} name='settings' size={25} color='white'></Feather>
                    </TouchableOpacity>
                </View>
               
            </View>

            <View style={styles.body}>

                <View style={styles.viewText}>

                <Text style={{color: 'black', fontSize: 15, fontWeight:'bold'}}> Notificações </Text>

                </View>

                {/* aqui é para adicionar a lista de notificações */}

                <View style={styles.viewNotifications}>

                </View>
                
            </View>

            <View style={styles.footer}>

                <View style={styles.container3}>

                <TouchableOpacity>
                    
                    <AntIcon name = 'user' size={30} color="white"/>

                </TouchableOpacity>
                    
                <TouchableOpacity onPress={() => navigation.navigate("GroupScreen")}>

                    <AntIcon name = 'team'size={30} color="white"/>

                </TouchableOpacity>

                <TouchableOpacity>

                    <MaterialIcons name = 'event' size={30} color="white"/>

                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate("ContactScreen")}>

                    <AntIcon name = 'contacts' size={30} color="white"/>

                </TouchableOpacity>

                </View>
            
            </View>

        </View>
    )
}

const {height} = Dimensions.get("screen");
const height_logo = height;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    container2:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: height - 520,
        top: 40,
        right: 5
    },    
    header:{
        flex: 0.1,
        backgroundColor: '#33b0b7',
        width: height,
        alignItems: 'center'
    },
    title:{
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
    },
    body:{
        flex: 0.9,
        backgroundColor: 'white',
        width: height,
        alignItems: 'center',
    },
    footer:{
        width: height,
        flex: 0.08,
        backgroundColor: '#33b0b7',
        alignItems: 'center',
    },
    container3:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: height- 550,
        top: 15
    },
    viewText:{
        top: 10,
        right: 140,
    },
    viewNotifications:{
        top: 20,
        borderWidth: 5,
        width: height - 480,
        alignItems: 'center',
        height: height - 229,
        borderColor: '#33b0b7'
    }
});