import React from 'react';

import { View, Text, TouchableOpacity, StatusBar, StyleSheet, Dimensions, FlatList, ScrollView} from 'react-native';

import * as Animatable from 'react-native-animatable';
import { LinearGradient } from 'expo-linear-gradient';
import AntIcon from "react-native-vector-icons/AntDesign";

import Onboarding from '../SlidingQuestionFolder/Onboarding'

const {width} = Dimensions.get("screen");
const height = width;

const FAQScreen = ({navigation}) =>{

    return(
        <View style={styles.container}>
            <StatusBar backgroundColor='#284b63' barStyle='light-content'></StatusBar> 
                <View style={styles.header}>
                    <Text style={styles.title}>
                        A AnyBody dá-lhe as boas vindas!
                    </Text>
                </View>
            <Animatable.View style={styles.footer} animation="fadeInDown" duration={1000}>
                <View>
                    <Onboarding />
                </View>
            </Animatable.View>
            <View style={styles.button}>
                <TouchableOpacity onPress={() => navigation.navigate('PerfilScreen')}>
                    <LinearGradient colors={['#a4c3b2','#a4c3b2']} style={styles.signIn}>
                        <Text style={styles.textSign}>CONCORDAR E CONTINUAR</Text>
                        <AntIcon style={styles.icon} name="rightcircle" color="#284b63" size ={15}/>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default FAQScreen;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#284b63'
    },
    header:{
        paddingTop: 40,
        alignSelf: 'center',
        justifyContent: 'center',
        paddingBottom: 35
    },
    footer:{
        flex: 2,
        backgroundColor: '#284b63',
        alignItems: 'flex-start',
        justifyContent: 'center',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        backgroundColor: 'white'
    },
    logo:{
        width: width,
        height: height  
    },
    title: {
        color: 'white',
        fontSize: 22,
        fontWeight: 'bold',
        flexDirection: 'row'
    },
    text: {
        color: 'grey',
        marginTop: 15
    },
    button:{
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingBottom: 40,
        paddingTop: 40
    },
    signIn: {
        width: 300,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomRightRadius: 8,
        borderBottomLeftRadius: 8,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,

    },
    textSign: {
        color: '#284b63',
    },
    icon:{
        paddingLeft: 15
    },
    textInSlide:{
        top: 5,
        width: width,
        height: height + 200,
    },
    scroll:{
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: "#284b63"
    },
    dotView:{
        flexDirection:'row',
        position: 'absolute',
        bottom:0,
        alignSelf: 'center',
    },
    dotText:{
        color:'#a4c3b2',
        margin: 3
    },
    dotTextActive:{
        color:'white',
        margin: 3
    }
});