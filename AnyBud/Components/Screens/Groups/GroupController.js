import React from 'react';

import GroupView from './GroupView';

export default GroupController = ({navigation}) => {

    return(

        <GroupView navigation = {navigation}/>
    )
}