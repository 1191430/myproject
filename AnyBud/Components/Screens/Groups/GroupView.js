import React from 'react';

import{
    View,
    Text,
    Button,
    StyleSheet,
    Dimensions,
    Image,
    StatusBar,
    TextInput,
    TouchableOpacity,
} from 'react-native';

import AntIcon from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import Ionicons from "react-native-vector-icons/Ionicons";
import SearchBarView from '../../ReusedComponents/SearchBar/SearchBarView';

export default GroupView = ({navigation}) => {

    return (

        <View style={styles.mainContainer}>

            <StatusBar 
                backgroundColor = '#33b0b7' 
                barStyle='light-content'
            >
            </StatusBar>

            <View style={styles.container}>

                <View style={{top: '15%', right:'41%'}}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Ionicons 
                                name="arrow-back" 
                                size={25} 
                                color='white'
                            >
                            </Ionicons>
                    </TouchableOpacity>
                </View>

                <View style={styles.container2}>

                    <Text style={styles.title}> Anybody </Text>

                    {/* <SearchBarView/> */}

                    <TouchableOpacity>
                        <Feather 
                            name='settings' 
                            size={25} 
                            color='white'
                            style={{right: 4}}
                        >
                        </Feather>

                    </TouchableOpacity>
                </View>

            </View>

            <View style={{flex: 0.9}}>

                <View style={styles.body}>

                    <View style={styles.TextPosition}>

                        <Text style={{color: 'black', fontSize: 15, fontWeight:'bold',}}> GRUPOS </Text>

                    </View>

                    {/* aqui é para adicionar a lista de grupos */}

                    <View style={styles.viewNotifications}>

                    </View>

                    <TouchableOpacity style={styles.IconAddUserPosition}>

                        <View style={styles.IconAddUser}>
                            
                            <AntIcon
                                name="addusergroup" 
                                color="white" 
                                size = {40}
                                style = {{paddingTop: 3}}
                            >
                            </AntIcon>
                        </View>

                    </TouchableOpacity>

                </View>
                    
            </View> 

        </View>      
    )
};

const {height} = Dimensions.get("screen");
const height_logo = height;

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
    },  
    container:{
        flex: 0.1,
        backgroundColor: '#33b0b7',
        alignItems: 'center',
    },
    container2:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        top: '4.5%',
        width: '90%'
    },
    title:{
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
    },
    newContainer:{
        flex:0.8,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    body:{
        paddingTop: '3%',
        backgroundColor: 'white',
        alignItems: 'center',
        height: '100%'
    },
    IconAddUser:{
        borderWidth: 2,
        width: 50,
        height: 50,
        alignItems: 'center',
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        backgroundColor: '#33b0b7',
        borderColor: '#33b0b7',
    },
    IconAddUserPosition:{
        position: 'absolute',
        bottom: '5%',
        right: '10%'
    },
    TextPosition:{
        right:'36%',
    }
});
