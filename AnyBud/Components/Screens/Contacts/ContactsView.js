import React from "react";

import{
    View,
    Text,
    Button,
    StyleSheet,
    Dimensions,
    Image,
    StatusBar,
    TextInput,
    TouchableOpacity,
    FlatList,
    ScrollView,
    Modal
} from 'react-native';

import AntIcon from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import Ionicons from "react-native-vector-icons/Ionicons";

export default ContactsView = ({navigation, contacts, ...props}) => {

    return (
        
        <View style={styles.mainContainer}>

            <StatusBar 
                backgroundColor = '#33b0b7' 
                barStyle='light-content'
            >
            </StatusBar>

            <View style={styles.container}>

                <View style={{top: '15%', right:'41%'}}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Ionicons 
                                name="arrow-back" 
                                size={25} 
                                color='white'
                            >
                            </Ionicons>
                    </TouchableOpacity>
                </View>

                <View style={styles.container2}>

                    <Text style={styles.title}> Anybody </Text>

                    {/* <SearchBarView/> */}

                    <TouchableOpacity>
                        <Feather 
                            name='settings' 
                            size={25} 
                            color='white'
                            style={{right: 4}}
                            >
                        </Feather>

                    </TouchableOpacity>
                </View>

            </View>

            <View style={styles.body}>

                <View style={styles.viewText}>

                <Text style={{color: 'black', fontSize: 15, fontWeight:'bold'}}> Contactos </Text>

                </View>

                {/* aqui é para adicionar a lista de contactos */}

                <View style={styles.viewNotifications}>

                <FlatList style={{height: '92%', width: '100%'}}
                    data={contacts}
                    renderItem={({item, index}) => {
                    return (
                        <View key={index}
                            style={{backgroundColor: 'white', borderBottomColor:'#33b0b7', borderBottomWidth: 2, paddingBottom: 5}}
                        >
                            <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                                    <Text style={{top: 10,left: 10,fontSize: 18, color:'black', textAlign: 'left', fontWeight:'bold'}}>
                                        {item.name}
                                    </Text>
                                    
                                    {/* aqui é para adicionar a imagem dos contactos */}

                                    <View style ={{borderColor: '#33b0b7'}}>
                                        <Image style = {styles.icon}source={{uri: 'https://digimedia.web.ua.pt/wp-content/uploads/2017/05/default-user-image.png'}}></Image>
                                    </View>
                            </View>

                            {/* aqui é para mostrar os números dos contactos */}

                            <Text style={{left: 10, fontSize: 15, color:'black', textAlign: 'left'}}> 
                                {item.phoneNumbers && item.phoneNumbers[0] && item.phoneNumbers[0].number}
                            </Text>
                        </View>
                    )
                }}
                keyExtractor={item => item.id}
                />
                </View>

            </View>

            <TouchableOpacity style={styles.IconImportContactPosition}>
                   
                {console.log(contacts)}

                <View style={styles.IconAddUser}>

                    <AntIcon
                        name="adduser" 
                        color="white" 
                        size = {40}
                        style = {{paddingTop: 3}}
                    >
                    </AntIcon>

                </View>

            </TouchableOpacity>

        </View>
    );

};

const {height} = Dimensions.get("screen");
const height_logo = height;

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
    },  
    container:{
        flex: 0.1,
        backgroundColor: '#33b0b7',
        alignItems: 'center',
    },
    container2:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        top: '4.5%',
        width: '90%'
    },
    title:{
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
    },
    body:{
        flex: 0.9,
        backgroundColor: 'white',
        width: height,
        alignItems: 'center',
    },
    viewText:{
        top: 10,
        right: "27%"
    },
    viewNotifications:{
        top: 20,
        borderWidth: 5,
        right:'27%',
        width: "42%",
        alignItems: 'center',
        borderColor: '#33b0b7',
    },
    IconAddUser:{
        borderWidth: 2,
        width: 50,
        height: 50,
        alignItems: 'center',
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        backgroundColor: '#33b0b7',
        borderColor: '#33b0b7',
    },
    IconAddUserPosition:{
        position: 'absolute',
        bottom: '3.5%',
        right: '8%'
    },
    IconImportContactPosition:{
        position: 'absolute',
        bottom: '4.5%',
        right: '10%'
    },
    icon:{
      width: 40,
      height: 40,
      borderTopLeftRadius: 60,
      borderTopRightRadius: 60,
      borderBottomRightRadius: 60,
      borderBottomLeftRadius: 60,
      right: 10,
      top: 14
    }
});
