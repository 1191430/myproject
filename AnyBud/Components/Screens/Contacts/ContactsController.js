import React, { useState, useEffect } from "react";

import{
    Text,
} from 'react-native';

import ContactsView from "./ContactsView";
import * as Contacts from 'expo-contacts';

export default ContactsController = ({navigation}) => {

    // pedir permissões para fazer o acesso aos contactos

    const [hasPermission, setHasPermission] = useState(null);
    const [contacts, setContacts] = useState([]);
    const [showModal, setShoowModal] = useState(true);

    useEffect(() => {
         (async () => {
             const contactStatus = await Contacts.requestPermissionsAsync();
             setHasPermission(contactStatus.status);

             if(hasPermission === 'granted'){

                const {data} = await Contacts.getContactsAsync({
                    fields:[Contacts.Fields.PhoneNumbers]
                });
    
                if(data.length > 0){
                    setContacts(data);
                }
            }
         })();
    },[hasPermission]);

    if(hasPermission !== 'granted'){
        return(
            <Text> Sem acesso à lista de contactos </Text>
        )
    }

    return(
        contacts !== null ? (
            <ContactsView
             navigation={navigation}
             hasPermission = {hasPermission}
             contacts = {contacts}
             image = {image}
            /> 
       ) : <LoadingPhrase/>
    );
};