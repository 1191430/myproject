import React from 'react';

import{
    View,
    Text,
    Button,
    StyleSheet,
    Dimensions,
    Image,
    StatusBar,
    TextInput,
    TouchableOpacity,
} from 'react-native';

import AntIcon from "react-native-vector-icons/AntDesign";
import { LinearGradient } from 'expo-linear-gradient';

export default LoginView = ({navigation, data, ...props}) => {

    return(
        <View style = {styles.container}>

            <StatusBar 
                backgroundColor = 'white' 
                barStyle='dark-content'
            >
            </StatusBar>

            <View style = {styles.header}>

                    <Image
                        source={require('./assets/logo_size.jpg')}
                        style = {styles.logo}
                    /> 

                    <View style={{flexDirection: 'row', borderBottomWidth:2, borderBottomColor:'#33b0b7'}}>

                        <AntIcon style={styles.Email} name='mail' size={20} color='grey'>

                        </AntIcon>

                        <TextInput
                            style={styles.textInputNome} 
                        >

                        </TextInput>

                    </View>

                    <View style={{flexDirection: 'row', borderBottomWidth:2, top: 30, borderBottomColor:'#33b0b7'}}>

                        <AntIcon 
                            style={styles.Email} 
                            name='lock' 
                            size={20} 
                            color='grey'

                        />

                        <TextInput
                            style={styles.textInputNome}
                            secureTextEntry={data.secureTextEntry ? true : false}
                            autoCapitalize="none"
                            onChangeText = {(val) => props.handlePasswordChange(val)}
                            
                        />

                        <TouchableOpacity onPress={props.updateSecureTextEntry}>

                            {data.secureTextEntry ? 

                                <AntIcon 
                                    style={styles.Eye} 
                                    name='eyeo' 
                                    color='grey' 
                                    size={20} 
                                />      
                                :
                                <AntIcon 
                                    style={styles.Eye} 
                                    name='eye' 
                                    color='#33b0b7' 
                                    size={20} 
                                />    
                            }

                        </TouchableOpacity>
                    </View>
            </View>

            <LinearGradient  style={styles.footer} colors={['white', '#33b0b7']}>

                <TouchableOpacity onPress={() => navigation.navigate('HomeScreen')} style = {styles.button}>
                            
                    <LinearGradient style = {{width: 250, 
                            paddingBottom:8.5, 
                            paddingTop:5, 
                            borderTopLeftRadius: 4,
                            borderTopRightRadius: 4,
                            borderBottomRightRadius: 4,
                            borderBottomLeftRadius: 4}} 
                            colors={['white', 'white']}
                        >
                        <Text style={{textAlign: 'center', fontSize: 17, color:'#33b0b7', fontWeight:'bold'}}> Log In</Text>

                    </LinearGradient>

                </TouchableOpacity>

            </LinearGradient>

        </View>
    );
};

const {height} = Dimensions.get("screen");
const height_logo = height;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        paddingTop: 60
    },
    header:{
        alignContent: 'center',
        justifyContent: 'center',
    },
    footer:{
        top: 100,
        width: height,
        flex: 1,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        height: 200,
        width: 200,
        paddingBottom: 15,
        left: 30
    },
    title:{
        color:'black',
        fontSize: 30,
        fontWeight:'bold'
    },
    text:{
        color: 'grey',
        marginTop: 5
    },
    button:{
        marginTop: 30,
    },
    signIn:{
        width: 150,
        height: 40,
        justifyContent: 'center',
        alignItems:'center',
        borderRadius: 50,
        flexDirection: 'row'
    },
    textSign:{
        color: 'black',
        fontWeight: 'bold'
    },
    Email:{
        top: 2,
        borderBottomWidth:2,
        borderBottomColor:'#33b0b7',
    },
    textInputNome:{
        width: 210,
        textAlign: 'center'
    },
    Eye:{
        top:5,
    }
});

