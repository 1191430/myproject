import React, { useEffect, useState } from 'react';

import LoginView from './LoginView'

export default LoginController = ({navigation}) => {

    // previne o retrocesso ao screen anterior
    React.useEffect(
        () => navigation.addListener('beforeRemove', (e) => {
            e.preventDefault();
        })
    )

    const [data, setData] = React.useState({
        password: "",
        secureTextEntry: true
    });

    const handlePasswordChange = (val) => {
        setData({
            ...data,
            password: val
        });
    }

    const updateSecureTextEntry = () => {

        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    return(
        <LoginView 
            navigation={navigation}
            data={data}
            handlePasswordChange = {handlePasswordChange}
            updateSecureTextEntry = {updateSecureTextEntry}
        />
    )
}