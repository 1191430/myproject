import React, { useEffect, useState } from 'react';

import { View, Text, TextInput,TouchableOpacity, Image, ImageBackground, ScrollViewComponent, Picker, StatusBar, StyleSheet, Dimensions, Button } from 'react-native';
import { LinearTextGradient } from "react-native-text-gradient";

import { LinearGradient } from 'expo-linear-gradient';
import AntIcon from "react-native-vector-icons/AntDesign";
import DateTimePicker from "@react-native-community/datetimepicker";
import PhoneInput from 'react-native-phone-number-input';

export default PerfilView = ({navigation,data,...props}) => {

    return (
        <View style={styles.container}>
            <StatusBar 
                backgroundColor = '#284b63' 
                barStyle='light-content'
            >
            </StatusBar>

            <View style={styles.header}>
                <Text style={styles.headerTitle}> Detalhes do perfil </Text>
            </View>

            <LinearGradient 
                colors={['#284b63', '#a4c3b2']} 
                style={styles.button}
            >   
                <Text style={[styles.newText,{top: 5}]}> Insira o seu número de telemóvel </Text>

                <View style={{paddingTop: 15, alignItems:'center'}}>
                    <PhoneInput defaultCode = 'IN' withShadow 
                            containerStyle={{height: 59}} 
                            defaulValue = {props.phoneNumber}
                            onChangeFormattedText = {(text) => {
                            props.setNumber(text)
                            }}
                    >
                    </PhoneInput>
                    
                    <Text style ={{color: "red", fontStyle:"italic", paddingTop:10, textAlign:"center"}}>{props.phoneError}</Text>
                    
                </View>

                <View style = {styles.middleTextPosition}>
                        
                    <Text style={[styles.newText,{left: 15}]}> Forneça os restante dados necessários </Text>

                    <View style={[{flexDirection:"row", paddingTop: 20}]}>
                        <AntIcon 
                            style={styles.AntIcon} 
                            name="user" 
                            size={15} 
                            color="white"
                        >
                        </AntIcon>
                        <TextInput 
                            style={styles.textInputNome} 
                            placeholder={props.nome}
                            placeholderTextColor="black"
                            onChangeText = {nome => props.setNome(nome)}
                            value = {props.nome}
                            onBlur = {() => props.nameValidation()}
                        >
                        </TextInput>
                    
                    </View>
                    
                    <Text style ={{color: "red", fontStyle:"italic", paddingTop: 5, textAlign:"center", marginLeft:35}}>{props.nameError}</Text>

                    <View style={[{flexDirection:"row", paddingTop: 5, left: 10}]}>
                            <AntIcon style={styles.AntIcon} name = "lock" size={15} color="white"></AntIcon>
                            <TextInput 
                                    style={styles.textInputNome}
                                    placeholder = {data.password} 
                                    placeholderTextColor="black"
                                    onBlur={() => props.passwordValidation()}
                                    secureTextEntry = {data.secureTextEntry ? true : false}
                                    onChangeText = {(val) => props.handlePasswordChange(val)}
                                    value = {data.password}
                                >
                            </TextInput>
                            <TouchableOpacity style={{top: 5, left: 5}} onPress={props.updateSecureTextEntry}>

                                {  data.secureTextEntry ? 

                                    <AntIcon 
                                        style={styles.Eye} 
                                        name='eyeo' 
                                        color='white' 
                                        size={20} 
                                    />      
                                    :
                                    <AntIcon 
                                        style={styles.Eye} 
                                        name='eye' 
                                        color='#33b0b7' 
                                        size={20} 
                                    />    
                                }

                            </TouchableOpacity>
                    </View>

                    <Text style ={{color: "red", fontStyle:"italic", paddingTop: 5, textAlign:"center", marginLeft:35}}>{props.passwordError}</Text>

                    <View style={[{flexDirection:"row", paddingTop: 5}]}>
                                
                            <AntIcon style={styles.AntIcon} name = "mail" size={15} color="white"></AntIcon>
                            <TextInput 
                                style={styles.textInputNome} 
                                placeholder={props.email}
                                onBlur={() => props.emailValidation()}
                                placeholderTextColor="black"
                                onChangeText = {email => props.setEmail(email)}
                                value = {props.email}
                            >
                            </TextInput>

                    </View>

                    <Text style ={{color: "red", fontStyle:"italic", paddingTop: 5, textAlign:"center", marginLeft:35}}>{props.emailError}</Text>

                    <View style={{paddingTop: 5, right: 14.5}}>
                        
                        <View style={{flexDirection:'row'}}>
                    
                                <AntIcon style={styles.AntIconDate} name = "calendar" size={15} color="white"></AntIcon>

                                <TouchableOpacity onPress={props.showMode}>
                                    <LinearGradient style={styles.dataLinear} colors={['transparent', 'transparent']}>
                                        <Text style={{fontSize: 14, textAlign:'center', paddingTop: 5, paddingBottom:5}}>{props.texto}</Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                                { props.show && 
                                    <DateTimePicker
                                        testID = 'dateTimePicker'
                                        value = {props.date}
                                        mode = {props.mode}
                                        onChange={props.onChange}
                                        
                                    /> 
                                }     
                        </View>
                    </View>

                        <Text style={[styles.buttonText,{textAlign:'center', paddingTop:20, left: 15}]}> Forneça uma foto de perfil </Text>

                        <TouchableOpacity onPress={props.pickImage} style={{alignItems:'center', left: 15, paddingTop: 10}}>
                        {
                            props.image && <Image source={{uri: props.image}} style={styles.icon}/>
                        }
                        </TouchableOpacity>
                </View>        

                <Text style={styles.finalText}> Tem de ter, pelo menos, <Text style={{color: '#284b63'}}>16 anos de idade</Text> para se registar. 
                    Esses são os nossos termos de registo.
                </Text>
                        
                <TouchableOpacity onPress={() => props.verificarRegisto()} style={styles.nextButtonPosition}>
                    <LinearGradient style={styles.nextButtonInside} colors={['#284b63', '#284b63']}>
                        <Text style={styles.nextButtonText}>SEGUINTE</Text>
                    </LinearGradient>
                </TouchableOpacity>

                <Text style ={{color: "red", fontStyle:"italic", paddingTop: 5, textAlign:"center", marginLeft:10, width:'80%'}}>{props.registerError}</Text>

            </LinearGradient>
        </View>
    )
}


const { height } = Dimensions.get("screen");
const width = {height};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#284b63',
        textAlign: 'center',
    },
    header: {
        justifyContent: 'center',
        textAlign: 'center',
    },
    headerTitle: {
        justifyContent: 'center',
        alignContent: 'center',
        textAlign: 'center',
        fontWeight: 'bold',
        paddingTop: 20,
        fontSize: 16,
        color: 'white',
        paddingBottom: 15
    },
    buttonText: {
        fontSize: 14,
        paddingTop: 10,
        paddingBottom: 15,
        color: 'white'
    },
    newText:{
        color: 'white',
        fontSize: 14,
        paddingTop: 10,
        textAlign: 'center',
    },
    button: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        alignItems: 'center',
        alignContent: 'center'
    },
    containerPhoneInside:{
        height: 60,
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
    },
    nextButtonPosition:{
        left: 5
    },
    nextButtonInside:{
        width:100,
        alignContent:'center',
        alignItems:'center',
        justifyContent: 'center',
        padding: 10,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        borderBottomLeftRadius: 4,
        right: 5,
    },
    nextButtonText:{
        fontSize: 13,
        color:'white'
    },
    finalText:{
        color:'white',
        padding: 20,
        paddingBottom: 10,
        paddingTop: 20,
        textAlign:'center',
    },
    icon:{
      width: 120,
      height: 120,
      borderTopLeftRadius: 60,
      borderTopRightRadius: 60,
      borderBottomRightRadius: 60,
      borderBottomLeftRadius: 60,
    },
    middleTextPosition:{
      alignItems:'center',
      right: 15
    },
    textInputNome:{
      borderWidth: 2,
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
      borderBottomRightRadius: 5,
      borderBottomLeftRadius: 5,
      borderColor: 'white',
      width: 240,
      color: "black",
      textAlign: 'center'
    },
    AntIcon:{
        left:20,
        paddingRight: 25,
        paddingTop: 7,
        borderColor: 'white',
    },
    nextButtonDate:{
        width: 200,
        
    },
    buttonDate:{
        textAlign: 'center',
        backgroundColor: 'transparent'
    },
    newButton:{
        backgroundColor:'#1E6738'
    },
    dataLinear:{
        borderWidth:2,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        left: 27,
        width: 240,
        borderColor:'white'
    },
    AntIconDate:{
        left:23,
        paddingTop:8,
    },
    containerPhoneInside:{
        height: 60,
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
    },
});