import React, { useEffect, useState } from 'react';

import * as ImagePicker from 'expo-image-picker';
import PerfilView from './PerfilView';
import axios from 'axios';

export default PerfilController = ({navigation}) => {

    // previne o retrocesso ao screen anterior
    React.useEffect(
        () => navigation.addListener('beforeRemove', (e) => {
            e.preventDefault();
        })
    )

    //para definir a data de nascimento
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);
    const [texto, setTexto] = useState("Insira a data de nascimento");
    const [nome, setNome] = useState("Primeiro e Ultimo Nome");
    const [email, setEmail] = useState("Insira o seu email");
    const [emailError, setEmailError] = useState("");
    const [passwordError, setPasswordError] = useState("");
    const [nameError, setNameError] = useState("");
    const [phoneError, setPhoneError] = useState("");
    const [isValid1, setValidation1] = useState(99);
    const [isValid2, setValidation2] = useState(99);
    const [isValid3, setValidation3] = useState(99);
    const [registerError, setRegisterError] = useState("");

    //handle da palavra - passe 

    const [data, setData] = useState({
        password: "Insira a palavra-passe",
        secureTextEntry: true
    });

    const handlePasswordChange = (val) => {
        setData({
            ...data,
            password: val
        });
    }

    const updateSecureTextEntry = () => {

        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    //para definir o número do utilizador

    const [phoneNumber, setNumber] = useState(0);   

    const onChange = (event, selectedDate) => {
        
        const currentDate = selectedDate || date;
        
        setShow(!show);

        setDate(currentDate);

        let tempDate = new Date(currentDate);
        let fDate = tempDate.getDate() + '/' + (tempDate.getMonth() + 1) + '/' + tempDate.getFullYear();

        setTexto(fDate);
    }

    const showMode = () => {
        setShow(true);
    }

    //para definir como inserir a imagem do perfil

    const [hasGalleryPermission, setHasGalleryPermission] = useState(null);
    const [image, setImage] = useState('https://digimedia.web.ua.pt/wp-content/uploads/2017/05/default-user-image.png');

    useEffect(() => {
         (async () => {
             const galleryStatus = await ImagePicker.requestMediaLibraryPermissionsAsync();
             setHasGalleryPermission(galleryStatus.status);
         })();
    },[]);

    const pickImage = async () => {
        
        if(hasGalleryPermission === 'granted'){
            
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                allowsEditing: true,
                aspect:[4,3],
                quality:1,
            });
   
            console.log(result)
   
            if(!result.cancelled){
                setImage(result.uri);
            }

        }    
    };

    if(hasGalleryPermission === false){
        return <Text>Sem acesso à galeria de imagens</Text>
    }

    // verificar o registo
    const verificarRegisto = () => {

        // verifica se a data é válida

        var error = dataValidation();

        if(error === 'Not valid'){

            setRegisterError("A data que inseriu não é válida. Por favor altere-a.");
        }

        // verifica se os outros dados foram bem inseridos

        else if(isValid1 != 0 || isValid2 != 0 || isValid3 != 0){

            setRegisterError("Um dos campos não está de acordo com o pedido. Por favor altere-o.");
        }

        // verifica se o número de telefone é válido

        else if(phoneError === 'Not valid' || phoneError === 'Error'){
            
            setRegisterError(" O número de telefone que colocou já se encontra registado.");
        }

        else {
            // registerUser();
            navigation.navigate("LoginScreen");
        }
    }

    const numberValidaton = (number) => {

        //verificar se ele já não se encontra na base de dados

    }

    const dataValidation = () => {

        var atualDate = new Date(getCurrentDate());
        var dias = Math.floor((Math.abs(date-atualDate))/(1000*60*60*24));
        var anos = dias / 365;
        
        if(anos < 16){
            return "Not valid";
        }

        else if(date.getTime() > atualDate.getTime()){
            return "Not valid";
        }

        else{
            return "Valid";
        }
    }

    const getCurrentDate = () => {

        var data = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();

        var completeDate = month + '/' + data + '/' + year; //format: dd/mm/yyyy;

        return completeDate;
    }

    const emailValidation = () => {

        let regex = new RegExp(/^[a-zA-Z0-9][a-zA-Z0-9._-]*@[a-zA-Z0-9][a-zA-Z0-9._-]*\.[a-zA-Z]{2,4}$/);

        if(!regex.test(email)){
            
            setEmailError("O campo do email terá de seguir o formato: algo@algo.com");
            setValidation1(1);
        }

        // implementar request para ver se ele existe ou não na base de dados

        // else if() {

        // }

        else{
            setEmailError("");
            setValidation1(0);
        }

    }

    const passwordValidation = () => {

        let regex = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/);

        if(!regex.test(data.password)){
            
            setPasswordError("A password deve conter: 1 caracter especial, 1 letra maiuscula e 1 um número, tem de ter no mínimo 8 digitos.");
            setValidation2(2);
        }

        else{
            setPasswordError("");
            setValidation2(0);
        }
    }  

    const nameValidation = () => {

        let rjx = new RegExp(/^([a-zA-Z]{2,}\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/);
    
        if(!rjx.test(nome)){
            
             setNameError("Escrever o primeiro e último nome.");
             setValidation3(3);
        }

        else {    
            setNameError("");
            setValidation3(0);
        }

    }

    // método para obter a validação do número

    const getPhoneVerification = async (number) =>{
      await axios({
            url: '',
            method: 'GET',
            headers:{
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then( res => {
            if(res.ok){

                if(res.json().length > 0){
                    setPhoneError("Not valid");
                }

                else{
                    setPhoneError("Valid");
                }
            }
            else{
                setPhoneError("Error");
            }
        })
    }

    // método para obter a validação do email

    const getEmailVerification = async (email) =>{
        await axios({
            url:'',
            method:'GET',
            headers:{
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(res => {
            if(res.ok){

                if(res.json().length > 0){
                    setPhoneError("Not valid");
                }

                else{
                    setPhoneError("Valid");
                }
            }
            else{
                setPhoneError("Error");
            }
        })
    }

    // método para fazer o registo do utilizador

    const registerUser = () =>{
        axios({
            url: '',
            method: 'POST',
            data:{
                email: email,
                phoneNumber: phoneNumber,
                nome: nome,
                password: password,
                avatar: image,
                nascimento: texto
            },
            headers:{
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(function(response){
            console.log(response);
        }).catch(function(error){
            console.log(error);
        });
    }

    // para enviar a informação necessária para a view
    return ( 
        <PerfilView
            hasGalleryPermission={hasGalleryPermission}
            setHasGalleryPermission={setHasGalleryPermission} 
            image={image}
            date={date}
            show={show}
            mode={'date'}
            texto={texto}
            navigation={navigation}
            onChange={onChange}
            showMode={showMode}
            setDate={setDate}
            setTexto={setTexto}
            setShow={setShow}
            pickImage={pickImage}
            phoneNumber={phoneNumber}
            setNumber={setNumber}
            nome={nome}
            setNome = {setNome}
            email = {email}
            setEmail = {setEmail}
            registerUser = {registerUser}
            handlePasswordChange = {handlePasswordChange}
            updateSecureTextEntry = {updateSecureTextEntry}
            data={data}
            nameValidation = {nameValidation}
            passwordValidation = {passwordValidation}
            emailValidation = {emailValidation}
            verificarRegisto = {verificarRegisto}
            emailError = {emailError}
            passwordError = {passwordError}
            nameError = {nameError}
            phoneError = {phoneError}
            registerError = {registerError}
            setPasswordError = {setPasswordError}
            setNameError = {setNameError}
        />  
    )
}
