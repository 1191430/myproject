import React, { useEffect, useState } from 'react';

import NumberView from './NumberView'

export default NumberController = ({navigation}) =>{

    return(
        <NumberView 
            navigation={navigation}
        />
    )
}