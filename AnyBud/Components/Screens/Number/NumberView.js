import React, { useState } from 'react';

import { View, Text, TextInput,TouchableOpacity, Image, ImageBackground, ScrollViewComponent, Picker, StatusBar, StyleSheet, Dimensions } from 'react-native';

import { LinearGradient } from 'expo-linear-gradient';
import PhoneInput from 'react-native-phone-number-input';

export default NumberView = ({ navigation, route, ...props }) => {

    const [phoneNumber, setNumber] = useState(0);

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor = '#284b63' barStyle='light-content'></StatusBar>
            <View style={styles.header}>
                <Text style={styles.headerTitle}> Digite o número de telemóvel </Text>
            </View>

            <LinearGradient colors={['#284b63', '#a4c3b2']} style={styles.button}>

                <Text style={styles.buttonText}> A Anybody precisa de verificar o seu número de telemóvel. </Text>

                <PhoneInput defaultCode = 'IN' withShadow 
                containerStyle={styles.containerPhoneInside} 
                defaulValue = {phoneNumber}
                onChangeFormattedText = {(text) => {
                    setNumber(text)
                }}
                >
                </PhoneInput>

                {console.log(phoneNumber)}
               
                <TouchableOpacity onPress={() => {navigation.navigate('PerfilScreen')}} style={styles.nextButtonPosition}>
                    <LinearGradient style={styles.nextButtonInside} colors={['#284b63', '#284b63']}>
                        <Text style={styles.nextButtonText}>SEGUINTE</Text>
                    </LinearGradient>
                </TouchableOpacity>

                <Text style={styles.finalText}> Tem de ter, pelo menos, <Text style={{color: '#284b63'}}>16 anos de idade</Text> para se registar. 
                    Esses são os nossos termos de registo.
                </Text>

            </LinearGradient>
        </View>
    )
}

const { height } = Dimensions.get("screen");

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#284b63',
        textAlign: 'center',
    },
    header: {
        justifyContent: 'center',
        textAlign: 'center',
    },
    headerTitle: {
        justifyContent: 'center',
        alignContent: 'center',
        textAlign: 'center',
        fontWeight: 'bold',
        paddingTop: 20,
        fontSize: 16,
        color: 'white',
        paddingBottom: 15
    },
    buttonText: {
        fontSize: 14,
        paddingTop: 15,
        textAlign: 'center',
        padding: 15,
        paddingBottom: 25,
        color: 'white'
    },
    button: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        alignItems: 'center',
        alignContent: 'center'
    },
    containerPhoneInside:{
        height: 60,
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
    },
    nextButtonPosition:{
        top: 500,
    },
    nextButtonInside:{
        width:100,
        alignContent:'center',
        alignItems:'center',
        justifyContent: 'center',
        padding: 10,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        borderBottomLeftRadius: 4,
    },
    nextButtonText:{
        fontSize: 13,
        color:'white'
    },
    finalText:{
        color:'white',
        top: 525,
        padding: 15,
        paddingBottom: 1,
        paddingTop: 1,
        textAlign:'center',
    }

});