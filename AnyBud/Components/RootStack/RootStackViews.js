import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import FAQScreen from '../Screens/FAQScreen';
import PerfilController from '../Screens/Perfil/PerfilController';
import LoginController from '../Screens/Login/LoginController';
import HomeController from '../Screens/HomeScreen/HomeController';
import GroupController from '../Screens/Groups/GroupController';
import ContactsController from '../Screens/Contacts/ContactsController';

const RootStack = createStackNavigator();

const RootStackScreen = ({navigation, route}) => (
<RootStack.Navigator screenOptions={{headerMode: 'none'}} >
    <RootStack.Screen name = "FAQScreen" component = {FAQScreen}/>
    <RootStack.Screen name = "PerfilScreen" component = {PerfilController}/>
    <RootStack.Screen name = "LoginScreen" component= {LoginController}/>
    <RootStack.Screen name = "HomeScreen" component= {HomeController}/>
    <RootStack.Screen name = "GroupScreen" component={GroupController}/>
    <RootStack.Screen name = "ContactScreen" component={ContactsController}/>
</RootStack.Navigator>
);

export default RootStackScreen;