import RootStackScreen from "./Components/RootStack/RootStackViews";
import { NavigationContainer } from '@react-navigation/native';

const App = () => {
  
  return(
    <NavigationContainer>
      <RootStackScreen/>
    </NavigationContainer> 
  );
}

export default App;
